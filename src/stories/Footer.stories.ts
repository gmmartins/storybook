import { storiesOf, moduleMetadata } from '@storybook/angular';
import { FooterModule } from 'invencaoz-components';
import { RouterTestingModule } from '@angular/router/testing';

const modules = {
  imports: [FooterModule, RouterTestingModule],
};

storiesOf('Site/Rodapé', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-footer [logoFooter]="logoFooter" [listSocial]="listSocial" ></ivz-footer>',
    props: {
      logoFooter: "./assets/img/logo_footer.svg",
      listSocial: [
        {
          id: 1,
          name: 'Facebook',
          image: './assets/img/facebook.svg',
          link: 'https://www.facebook.com/invencaoz/'
        },
        {
          id: 2,
          name: 'Instagram',
          image: './assets/img/instagram.svg',
          link: 'https://www.instagram.com/invencaoz/'
        },
        {
          id: 3,
          name: 'Linkedin',
          image: './assets/img/linkedin.svg',
          link: 'https://www.linkedin.com/company/invencaoz/'
        },
        {
          id: 4,
          name: 'Behance',
          image: './assets/img/behance.svg',
          link: 'https://www.behance.net/invencaoz'
        }
      ]
    }
  }))