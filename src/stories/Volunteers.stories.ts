import { storiesOf, moduleMetadata } from '@storybook/angular';
import { VoluntaryCardModule } from 'invencaoz-components';

const modules = {
  imports: [VoluntaryCardModule],
};

storiesOf('Intranet/Voluntários', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-voluntary-card [dataVoluntary]="Voluntary"></ivz-voluntary-card>',
    props: {
      Voluntary: {
        image: "./assets/img/user.jpg",
        name: "Nome Sobrenome",
        profession: "Profissão na ong",
      }
    }
  }))