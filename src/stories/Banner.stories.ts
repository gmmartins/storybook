import { storiesOf, moduleMetadata } from '@storybook/angular';
import { BgImageHomeModule } from 'invencaoz-components';

const modules = {
  imports: [BgImageHomeModule],
};

storiesOf('Site/Banner', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-bg-image-home [itemBg]="itemBg"></ivz-bg-image-home>',
    props: {
      itemBg: {
        id: 1,
        title: 'Uma agência de marketing digital para ONGs!',
        textImage: 'Olá! Somos a',
        image: './assets/img/logo-banner.svg',
        classImage: 'bg-image-home',
      }
    }
  }))

