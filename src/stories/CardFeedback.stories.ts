import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CardFeedbackModule } from 'invencaoz-components';

const modules = {
  imports: [CardFeedbackModule],
};

storiesOf('Components/Card Feedback', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-cards-feedback [dataFeedback]="dataFeedback"></ivz-cards-feedback>',
    props: {
      dataFeedback: {
        title: `Sou um depoimento curto de uma ONG parceira! Adoro a Invenção Z. Sou um depoimento curto de uma ONG parceira! Adoro a Invenção Z.`,
        name: `name ong`,
        img: `./assets/img/user.jpg`,
      },
    }
  }))
