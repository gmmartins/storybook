import { storiesOf, moduleMetadata } from '@storybook/angular';
import { TitleDefaultModule } from 'invencaoz-components';

const modules = {
    imports: [TitleDefaultModule],
};

storiesOf('Components/Títulos', module)
    .addDecorator(moduleMetadata(modules))
    .add('Títulos', () => ({
        template: `
            <ivz-title-default title="Title xxl" class="heading--xxl"></ivz-title-default>
            <ivz-title-default title="Title xl" class="heading--xl"></ivz-title-default>
            <ivz-title-default title="Title md" class="heading--md"></ivz-title-default>
            <ivz-title-default title="Title sm" class="heading--sm"></ivz-title-default>
        `
    }))
    .add('Títulos intranet', () => ({
        template: '<ivz-title-default title="Title default intranet" class="heading-intranet"></ivz-title-default>'
    }))