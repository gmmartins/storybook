import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CardBirthdayModule } from 'invencaoz-components';

const modules = {
  imports: [CardBirthdayModule],
};

storiesOf('Intranet/Card Aniversáriante', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '  <ivz-card-birthday [dataBirthday]="birthdays"></ivz-card-birthday>',
    props: {
      birthdays: [
        {
          image: "./assets/img/user.jpg",
          name: "Nome Sobrenome",
          profession: "Profissão na ONG",
          date: "19"
        },
        {
          image: "./assets/img/user.jpg",
          name: "Nome Sobrenome",
          profession: "Profissão na ONG",
          date: "20"
        },
      ]
    }
  }))
