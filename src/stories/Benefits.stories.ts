import { storiesOf, moduleMetadata } from '@storybook/angular';
import { BenefitsModule } from 'invencaoz-components';

const modules = {
  imports: [BenefitsModule],
};

storiesOf('Site/Benefícios Voluntários', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-benefits [text]="text"></ivz-benefits>',
    props: {
      text: "Usar seu talento para ajudar uma ONG"
    }
  }))
  .add('Imagem', () => ({
    template: '<ivz-benefits [text]="text" [image]="imagem"></ivz-benefits>',
    props: {
      text: "Usar seu talento para ajudar uma ONG",
      imagem: "./assets/img/user.jpg"
    }
  }));
