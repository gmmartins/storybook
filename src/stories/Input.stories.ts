import { storiesOf, moduleMetadata } from '@storybook/angular';
import { InputCpfModule, InputDefaultModule } from 'invencaoz-components';

const modules = {
  imports: [InputCpfModule, InputDefaultModule],
};

storiesOf('Components/Input', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-input-default id="nome" label="Nome" placeholder="Ex.: Exemplo"></ivz-input-default>'

  }))
  .add('Mask CPF', () => ({
    template: '<ivz-input-cpf id="cpf" label="Cpf" placeholder="Ex.: 342.342.342-34"></ivz-input-cpf>'
  }))