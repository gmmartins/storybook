import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CardHomeModule } from 'invencaoz-components';

const modules = {
  imports: [CardHomeModule],
};

storiesOf('Intranet/Card bem vindo', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-card-home></ivz-card-home>',
  }))
  .add('Nome do usuário', () => ({
    template: '<ivz-card-home [name]="name"></ivz-card-home>',
    props: {
      name: "Gabriel Moraes"
    }
  }))