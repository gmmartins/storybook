import { storiesOf, moduleMetadata } from '@storybook/angular';
import { NavbarModule } from 'invencaoz-components';
import { RouterTestingModule } from '@angular/router/testing';

const modules = {
  imports: [NavbarModule, RouterTestingModule],
};

storiesOf('Site/Menu', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-navbar [menuOpen]="menuOpen" [menuClose]="menuClose" [urlImage]="urlImage" [itemsMenu]="itemsMenu" [color]="color"> </ivz-navbar>',
    props: {
      menuOpen: './assets/img/menu-open.svg',
      menuClose: './assets/img/menu-close.svg',
      urlImage: './assets/img/logo_mobile.svg',
      color: 'bg-blue',
      itemsMenu: [
        {
          id: 1,
          name: 'Home',
          routerLink: 'home',
          active: true
        },
        {
          id: 2,
          name: 'ONGs',
          routerLink: 'ongs',
          active: true
        },
        {
          id: 3,
          name: 'Voluntários',
          routerLink: 'volunteers',
          active: true
        },
        {
          id: 4,
          name: 'Como funciona',
          routerLink: 'howWorks',
          active: true
        },
        {
          id: 5,
          name: 'Sobre Nós',
          routerLink: 'about',
          active: true
        },
        {
          id: 6,
          name: 'Doe',
          routerLink: 'donate',
          active: true
        },
        {
          id: 7,
          name: 'Blog',
          routerLink: 'blog',
          active: true
        },
        {
          id: 8,
          name: 'Contato',
          routerLink: 'contact',
          active: true
        },
        {
          id: 9,
          name: 'Entrar',
          routerLink: 'login',
          active: true
        },
      ]
    }
  }))