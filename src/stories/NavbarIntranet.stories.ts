import { storiesOf, moduleMetadata } from '@storybook/angular';
import { NavbarIntranetModule } from 'invencaoz-components';
import { RouterTestingModule } from '@angular/router/testing';

const modules = {
  imports: [NavbarIntranetModule, RouterTestingModule],
};

storiesOf('Intranet/Menu', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-navbar-intranet [urlImage]="urlImage" [itemsMenu]="itemsMenu" [imgPerfil]="imgPerfil"></ivz-navbar-intranet>',
    props: {
      imgPerfil: './assets/img/user.jpg',
      urlImage: './assets/img/intranet-logo.svg',
      itemsMenu: [
        {
          id: 1,
          name: "Página inicial",
          routerLink: "home",
          active: true
        },
        {
          id: 2,
          name: "Nossa ONG",
          routerLink: "/ourOng",
          active: true
        },
        {
          id: 3,
          name: "Minhas coisas",
          routerLink: "/myStuff",
          active: true
        },
        {
          id: 4,
          name: "Educacional",
          routerLink: "/educational",
          active: true
        },
        {
          id: 5,
          name: "Suporte",
          routerLink: "/support",
          active: true
        }
      ]
    }
  }))