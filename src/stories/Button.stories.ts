import { storiesOf, moduleMetadata } from '@storybook/angular';
import { ButtonModule } from 'invencaoz-components';

const modules = {
  imports: [ButtonModule],
};

storiesOf('Components/Buttons', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-button></ivz-button>',
  }))
  .add('Orange', () => ({
    template: '<ivz-button [color]="color" [title]="title"></ivz-button>',
    props: {
      color: 'orange',
      title: 'Button orange'
    }
  }))
  .add('Blue', () => ({
    template: '<ivz-button [color]="color" [title]="title"></ivz-button>',
    props: {
      color: 'blue',
      title: 'Button blue'
    }
  }))
  .add('Pink', () => ({
    template: '<ivz-button [color]="color" [title]="title"></ivz-button>',
    props: {
      color: 'pink',
      title: 'Button pink'
    }
  }))
  .add('Dark gray', () => ({
    template: '<ivz-button [color]="color" [title]="title"></ivz-button>',
    props: {
      color: 'dark-gray',
      title: 'Button dark gray'
    }
  }))
  .add('Gray', () => ({
    template: '<ivz-button [color]="color" [title]="title"></ivz-button>',
    props: {
      color: 'gray',
      title: 'Button gray'
    }
  }))