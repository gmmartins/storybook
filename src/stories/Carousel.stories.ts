import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CarouselModule } from 'invencaoz-components';

const modules = {
  imports: [CarouselModule],
};

storiesOf('Intranet/Carousel', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-carousel [itensCarousel]="itens"></ivz-carousel>',
    props: {
      itens: [
        {
          id: 1,
          image: 'https://picsum.photos/800/450/?image=58',
          title: 'Dia dos avós',
          description: `Queremos homenagear todos os avós. Vô e Vó não medem esforços pelos netos e são responsáveis pelas melhores
          memórias afetivas que temos da infância. Então, aproveita e me conta qual a sua lembrança ou características
          favoritas do seus avós?`,
          button: {
            title: 'Sou um botão',
            color: 'orange',
          },
        },
        {
          id: 2,
          image: 'https://picsum.photos/800/450/?image=59',
          title: 'Dia dos pais',
          description: `Queremos homenagear todos os pais. Pai não medem esforços pelos filhos e são responsáveis pelas melhores
          memórias afetivas que temos da infância. Então, aproveita e me conta qual a sua lembrança ou características
          favoritas do seu pai?`,
          button: {
            title: 'Sou um botão',
            color: 'blue',
          },
        },
        {
          id: 3,
          image: 'https://picsum.photos/800/450/?image=60',
          title: 'Dia das mães',
          description: `Queremos homenagear todos as mães. Mãe não medem esforços pelos filhos e são responsáveis pelas melhores
          memórias afetivas que temos da infância. Então, aproveita e me conta qual a sua lembrança ou características
          favoritas da sua mãe?`,
          button: {
            title: 'Sou um botão',
            color: 'pink',
          },
        },
      ]
    }
  }))
