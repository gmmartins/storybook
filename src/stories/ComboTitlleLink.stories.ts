import { storiesOf, moduleMetadata } from '@storybook/angular';
import { ComboTitleLinkModule } from 'invencaoz-components';
import { RouterTestingModule } from '@angular/router/testing';

const modules = {
  imports: [ComboTitleLinkModule, RouterTestingModule],
};

storiesOf('Site/Combo titulo e link', module)
  .addDecorator(moduleMetadata(modules))
  .add('Completo', () => ({
    template: '<ivz-combo-title-link [title]="title" [subTitle]="subtitle" [description]="description" [link]="link" [path]="path"></ivz-combo-title-link>',
    props: {
      title: 'Para os voluntários',
      subtitle: 'Você coloca em prática as suas habilidades, enriquece seu currículo e ajuda uma ONG',
      description: 'Com os cinco projetos abaixo, sua ONG ganha visibilidade e o voluntário ganha experiência profissional com um projeto de verdade.',
      link: 'mais vantagens para ser um voluntário',
      path: '/'
    }
  }))
  .add('Simples', () => ({
    template: '<ivz-combo-title-link [title]="title" [subTitle]="subtitle"> ivz-combo-title-link>',
    props: {
      title: 'Para os voluntários',
      subtitle: 'Você coloca em prática as suas habilidades, enriquece seu currículo e ajuda uma ONG',
    }
  }))