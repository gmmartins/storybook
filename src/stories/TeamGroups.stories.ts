import { storiesOf, moduleMetadata } from '@storybook/angular';
import { TeamsGroupModule } from 'invencaoz-components';

const modules = {
  imports: [TeamsGroupModule],
};

storiesOf('Intranet/Time', module)
  .addDecorator(moduleMetadata(modules))
  .add('Default', () => ({
    template: '<ivz-teams-group [dataTeam]="team"></ivz-teams-group>',
    props: {
      team: {
        title: 'Diretoria Executiva',
        description: 'Responsável pelo planejamento,estratégias e gestão de todas as equipes É aqui que planejamos todas as ações internas e externas.',
        users: ['./assets/img/user.jpg', './assets/img/user.jpg', './assets/img/user.jpg']
      },
    }
  }))
