import { Component } from '@angular/core';

@Component({
  selector: 'ivz-storybook',
  template: `<h1>Welcome to storybook of invenção Z</h1>`,
  styles: []
})
export class AppComponent {
  title = 'storybook';
}
