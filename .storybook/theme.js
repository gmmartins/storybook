import { create } from '@storybook/theming/create';

const themeBase = 'light';
const blue = '#0E3C66';
const orange = '#EA582A';
const white = '#FFF';
const gray = '#AAA';
const borderRadius = 4;
const fontOpen = '"Open Sans", sans-serif';
const fontQuick = '"Quicksand", sans-serif';
const urlImage = 'http://www.invencaoz.org/assets/img/logo-ivz.svg';
const name = 'Invenção Z'

export default create({
  base: themeBase,

  colorPrimary: blue,
  colorSecondary: orange,

  // UI
  appBg: blue,
  appContentBg: '#DEE6ED',
  appBorderRadius: borderRadius,

  // Typography
  fontBase: fontOpen,
  fontCode: fontQuick,

  // Text colors
  textColor: orange,

  // Toolbar default and active colors
  barTextColor: white,
  barSelectedColor: orange,
  barBg: blue,

  // Form colors
  inputBg: '#eee',
  inputTextColor: blue,
  inputBorderRadius: borderRadius,

  brandTitle: name,
  brandImage: urlImage,
});