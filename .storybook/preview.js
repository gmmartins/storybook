
import { setCompodocJson } from "@storybook/addon-docs/angular";
import docJson from "../documentation.json";

import './reset.css';

setCompodocJson(docJson);
